let roomID;
let data;
if (typeof window !== 'undefined') {
  roomID = localStorage.getItem('room');
  data = JSON.parse(localStorage.getItem('data'));
}
const initialState = {
  response: data,
  data: '',
  myChoice: data?.player_choice,
  compChoice: data?.computer_choice,
  score: data?.score,
  result: data?.result,
  room_id: roomID,
  code: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'PLAY RPS':
      return {
        ...state,
        response: action.payload,
        myChoice: action.choice,
        compChoice: action.compChoice,
        result: action.result,
        score: action.score,
      };
    case 'CREATE ROOM':
      return {
        ...state,
        response: action.res,
        room_id: action.payload,
      };
    case 'LEADERBOARD':
      return {
        ...state,
        data: action.payload.data,
        code: action.payload.meta.code,
      };
    case 'RESET CHOICE':
      return {
        ...state,
        myChoice: action.payload,
        compChoice: action.payload,
        result: null,
        score: 0,
      };

    default:
      return state;
  }
}
