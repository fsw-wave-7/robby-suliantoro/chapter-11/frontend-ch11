let initToken;
if (typeof window !== 'undefined') {
  initToken = localStorage.getItem('accessToken');
}

export const initialState = {
  status: '',
  // KETIKA REFRESH, DEFAULT VALUE SELALU NGAMBIL DARI LOCAL STORAGE
  token: initToken,
  users: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'REGISTER':
      return {
        ...state,
        users: action.payload,
      };
    case 'LOGIN':
      return {
        ...state,
        users: action.payload,
        token: action.token,
      };
    case 'CLEANUP':
      return {
        ...state,
        users: action.payload,
      };
    case 'LOGOUT':
      return {
        ...state,
        users: action.payload,
        token: action.token,
      };

    case 'FORGOT':
      return {
        ...state,
        users: action.payload,
      };

    case 'RESET':
      return {
        ...state,
        users: action.payload,
      };

    default:
      return state;
  }
}
